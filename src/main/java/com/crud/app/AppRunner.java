package com.crud.app;

import com.crud.app.dao.JdbcUtils;
import com.crud.app.runner.ConsoleHelper;

public class AppRunner {

    public static void main(String[] args) {
        JdbcUtils.initDb();
        ConsoleHelper helper = ConsoleHelper.getHelper();
        helper.startConsoleApplication();
    }
}
