package com.crud.app.controller;

import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.model.BaseObject;

import java.util.List;

public interface Controller<T extends BaseObject> {

    void onCreate(T obj) throws NotUniqueNameException, NotUniqueIdException;

    T onGetById(int id) throws NoSuchIdException;

    List<T> onGetAll();

    void onUpdate(T obj) throws NotUniqueNameException, NotUniqueIdException;

    void onDelete(int id);
}
