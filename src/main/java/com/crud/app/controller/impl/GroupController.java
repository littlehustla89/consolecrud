package com.crud.app.controller.impl;

import com.crud.app.controller.Controller;
import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.model.Group;
import com.crud.app.service.GroupService;
import com.crud.app.service.impl.GroupServiceImpl;

import java.util.List;

public class GroupController implements Controller<Group> {

    protected GroupService service;

    public GroupController() {
        this.service = new GroupServiceImpl();
    }

    @Override
    public void onCreate(Group obj) throws NotUniqueNameException, NotUniqueIdException {
        service.createObject(obj);
    }

    @Override
    public Group onGetById(int id) throws NoSuchIdException {
        return service.getObjectById(id);
    }

    @Override
    public List<Group> onGetAll() {
        return service.getAll();
    }

    @Override
    public void onUpdate(Group obj) throws NotUniqueNameException, NotUniqueIdException {
        service.updateObject(obj);
    }

    @Override
    public void onDelete(int id) {
        service.deleteObject(id);
    }
}
