package com.crud.app.controller.impl;

import com.crud.app.controller.Controller;
import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.model.Provider;
import com.crud.app.service.ProviderService;
import com.crud.app.service.impl.ProviderServiceImpl;

import java.util.List;

public class ProviderController implements Controller<Provider> {

    protected ProviderService service;

    public ProviderController() {
        this.service = new ProviderServiceImpl();
    }

    @Override
    public void onCreate(Provider obj) throws NotUniqueNameException, NotUniqueIdException {
        service.createObject(obj);
    }

    @Override
    public Provider onGetById(int id) throws NoSuchIdException {
        return service.getObjectById(id);
    }

    @Override
    public List<Provider> onGetAll() {
        return service.getAll();
    }

    @Override
    public void onUpdate(Provider obj) throws NotUniqueNameException, NotUniqueIdException {
        service.updateObject(obj);
    }

    @Override
    public void onDelete(int id) {
        service.deleteObject(id);
    }
}
