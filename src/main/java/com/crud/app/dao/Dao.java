package com.crud.app.dao;

import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.model.BaseObject;

import java.util.List;

public interface Dao<T extends BaseObject> {

    void create(T object) throws NotUniqueIdException, NotUniqueNameException;

    T getById(int id) throws NoSuchIdException;

    List<T> getAll();

    void update(T object) throws NotUniqueNameException, NotUniqueIdException;

    void delete(int id);
}
