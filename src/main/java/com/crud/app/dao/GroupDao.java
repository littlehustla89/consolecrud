package com.crud.app.dao;

import com.crud.app.model.Group;

public interface GroupDao extends Dao<Group> {
}
