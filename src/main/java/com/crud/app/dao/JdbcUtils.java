package com.crud.app.dao;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcUtils {

    private JdbcUtils() {
    }

    public static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    public static final String USERNAME = "postgres";
    public static final String PASSWORD = "postgres";

    public static void initDb() {
        try (InputStream is = JdbcUtils.class.getResourceAsStream("/scripts/createTables.sql")) {
            String sql = IOUtils.toString(is, Charset.defaultCharset());
            executeScripts(sql);
            System.out.println("init executed successfully");
        } catch (IOException e) {
            System.out.println("error while create tables");
        }
    }

    public static void executeScripts(String... scripts) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             Statement statement = connection.createStatement()) {
            for (String script : scripts) {
                statement.execute(script);
            }
        } catch (SQLException e) {
            System.out.println("invalid sql scripts");
        }
    }
}