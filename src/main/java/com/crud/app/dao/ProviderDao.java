package com.crud.app.dao;

import com.crud.app.model.Provider;

public interface ProviderDao extends Dao<Provider> {
}
