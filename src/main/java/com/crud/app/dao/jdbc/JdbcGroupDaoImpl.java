package com.crud.app.dao.jdbc;

import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.model.Group;
import com.crud.app.dao.GroupDao;
import com.crud.app.model.Provider;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.crud.app.dao.JdbcUtils.*;

public class JdbcGroupDaoImpl implements GroupDao {

    private static final String SELECT_BY_ID = "SELECT * FROM group_table WHERE id=?";
    private static final String SELECT_ALL = "SELECT * FROM group_table";
    private static final String SELECT_BY_NAME = "SELECT * FROM group_table WHERE name=?";
    private static final String DELETE_BY_ID = "DELETE FROM group_table WHERE id=?";
    private static final String INSERT = "INSERT INTO group_table VALUES (?, ?, ?, ?)";
    private static final String INSERT_AUTO_ID = "INSERT INTO group_table (name, description, region) VALUES (?,?,?)";
    private static final String UPDATE_BY_ID = "UPDATE group_table SET name=?, description=?, region=? WHERE id=?";

    private static final String SELECT_PROVIDERS_BY_GROUP_ID = "SELECT p.* FROM provider_table p JOIN group_table g on g.id= p.group_id and g.id =?";
    private static final String DELETE_PROVIDERS = "DELETE FROM provider_table p where p.group_id =?";

    @Override
    public void create(Group group) throws NotUniqueIdException, NotUniqueNameException {
        PreparedStatement prstmt;
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {
            if (group.getId() == 0) {
                prstmt = connection.prepareStatement(INSERT_AUTO_ID);
                prstmt.setString(1, group.getName());
                prstmt.setString(2, group.getRegion());
                prstmt.setString(3, group.getDescription());
                prstmt.executeUpdate();
            } else if (existWithId(connection, group.getId())) {
                throw new NotUniqueIdException("ID " + group.getId() + " is not unique.");
            } else {
                prstmt = connection.prepareStatement(INSERT);
                prstmt.setInt(1, group.getId());
                prstmt.setString(2, group.getName());
                prstmt.setString(3, group.getRegion());
                prstmt.setString(4, group.getDescription());
                prstmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Group getById(int id) throws NoSuchIdException {
        Group group = null;
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID);
             PreparedStatement ps = connection.prepareStatement(SELECT_PROVIDERS_BY_GROUP_ID)) {

            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int groupId = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String region = resultSet.getString("region");
                String description = resultSet.getString("description");
                group = new Group(groupId, name, region, description);
            }
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<Provider> providers = new ArrayList<>();
            while (rs.next()) {
                int providerId = rs.getInt("id");
                String name = rs.getString("name");
                String providerDescription = rs.getString("provider_description");
                String serviceName = rs.getString("service_name");
                Long servicePrice = rs.getLong("service_price");

                providers.add(new Provider(providerId, name, providerDescription, serviceName, servicePrice));
            }
            rs.close();
            if (group != null) {
                group.setProviders(providers);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (group != null) {
            return group;
        } else {
            throw new NoSuchIdException("There is no record in \"groups\" with ID " + id + "\n");
        }

    }

    @Override
    public List<Group> getAll() {
        List<Group> list = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             Statement statement = connection.createStatement();
             PreparedStatement stmt = connection.prepareStatement(SELECT_PROVIDERS_BY_GROUP_ID);
             ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {

            while (resultSet.next()) {
                int groupId = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String region = resultSet.getString("region");
                String description = resultSet.getString("description");
                Group group = new Group(groupId, name, region, description);

                stmt.setInt(1, groupId);
                ResultSet rs = stmt.executeQuery();
                List<Provider> providers = new ArrayList<>();
                while (rs.next()) {
                    int providerId = rs.getInt("id");
                    String providerName = rs.getString("name");
                    String providerDescription = rs.getString("provider_description");
                    String serviceName = rs.getString("service_name");
                    Long servicePrice = rs.getLong("service_price");

                    providers.add(new Provider(providerId, providerName, providerDescription, serviceName, servicePrice));
                }
                rs.close();
                group.setProviders(providers);
                list.add(group);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void update(Group group) throws NotUniqueNameException, NotUniqueIdException {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             Statement statement = connection.createStatement();
             PreparedStatement prstmt = connection.prepareStatement(UPDATE_BY_ID);
             ResultSet rs = statement.executeQuery(SELECT_ALL)) {
            List<Integer> idList = new ArrayList<>();
            while (rs.next()) {
                idList.add(rs.getInt("id"));
            }
            if (!idList.contains(group.getId())) {
                create(group);
            } else {
                prstmt.setString(1, group.getName());
                prstmt.setString(2, group.getRegion());
                prstmt.setString(3, group.getDescription());
                prstmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(DELETE_BY_ID);
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PROVIDERS)) {

            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean existWithId(Connection connection, int id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        return rs.next();
    }

    private boolean existWithName(Connection connection, String name) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_BY_NAME);
        statement.setString(1, name);
        ResultSet rs = statement.executeQuery();
        rs.next();
        return name.equalsIgnoreCase(rs.getString("name"));
    }

}
