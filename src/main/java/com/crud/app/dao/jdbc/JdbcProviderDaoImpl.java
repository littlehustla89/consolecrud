package com.crud.app.dao.jdbc;

import com.crud.app.dao.ProviderDao;
import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.model.Provider;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.crud.app.dao.JdbcUtils.*;

public class JdbcProviderDaoImpl implements ProviderDao {

    private static final String SELECT_BY_ID = "SELECT * FROM provider_table WHERE id=?";
    private static final String SELECT_ALL = "SELECT * FROM provider_table";
    private static final String SELECT_BY_NAME = "SELECT * FROM provider_table WHERE name=?";
    private static final String DELETE_BY_ID = "DELETE FROM provider_table WHERE id=?";
    private static final String INSERT = "INSERT INTO provider_table VALUES (?, ?, ?, ?)";
    private static final String INSERT_AUTO_ID = "INSERT INTO provider_table (name, provider_description, service_name, service_price,group_id) VALUES (?,?,?,?,?)";
    private static final String UPDATE_BY_ID = "UPDATE provider_table SET name=?, provider_description=?, service_name=?,  service_price=?, group_id=? WHERE id=?";


    @Override
    public void create(Provider provider) throws NotUniqueIdException, NotUniqueNameException {
        PreparedStatement prstmt;
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {
            if (provider.getId() == 0) {
                prstmt = connection.prepareStatement(INSERT_AUTO_ID);
                prstmt.setString(1, provider.getName());
                prstmt.setString(2, provider.getProviderDescription());
                prstmt.setString(3, provider.getServiceName());
                prstmt.setLong(4, provider.getServicePrice());
                prstmt.setInt(5, provider.getGroupId());
                prstmt.executeUpdate();
            } else if (existWithId(connection, provider.getId())) {
                throw new NotUniqueIdException("ID " + provider.getId() + " is not unique.");
            } else {
                prstmt = connection.prepareStatement(INSERT);
                prstmt.setInt(1, provider.getId());
                prstmt.setString(2, provider.getName());
                prstmt.setString(3, provider.getProviderDescription());
                prstmt.setString(4, provider.getServiceName());
                prstmt.setLong(5, provider.getServicePrice());
                prstmt.setInt(6, provider.getGroupId());
                prstmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Provider getById(int id) throws NoSuchIdException {
        Provider provider = null;
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {

            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int providerId = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String providerDescription = resultSet.getString("provider_description");
                String serviceName = resultSet.getString("service_name");
                Long servicePrice = resultSet.getLong("service_price");
                Integer groupId = resultSet.getInt("group_id");
                provider = new Provider(providerId, name, providerDescription, serviceName, servicePrice);
                provider.setGroupId(groupId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (provider != null) {
            return provider;
        } else {
            throw new NoSuchIdException("There is no record in \"groups\" with ID " + id + "\n");
        }

    }

    @Override
    public List<Provider> getAll() {
        List<Provider> list = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {

            while (resultSet.next()) {
                int providerId = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String providerDescription = resultSet.getString("provider_description");
                String serviceName = resultSet.getString("service_name");
                Long servicePrice = resultSet.getLong("service_price");
                Integer groupId = resultSet.getInt("group_id");
                Provider provider = new Provider(providerId, name, providerDescription, serviceName, servicePrice);
                provider.setGroupId(groupId);
                list.add(provider);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void update(Provider provider) throws NotUniqueNameException, NotUniqueIdException {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             Statement statement = connection.createStatement();
             PreparedStatement prstmt = connection.prepareStatement(UPDATE_BY_ID);
             ResultSet rs = statement.executeQuery(SELECT_ALL)) {
            List<Integer> idList = new ArrayList<>();
            while (rs.next()) {
                idList.add(rs.getInt("id"));
            }
            if (!idList.contains(provider.getId())) {
                create(provider);
            } else {
                prstmt.setString(1, provider.getName());
                prstmt.setString(2, provider.getProviderDescription());
                prstmt.setString(3, provider.getServiceName());
                prstmt.setLong(4, provider.getServicePrice());
                prstmt.setInt(5, provider.getGroupId());
                prstmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(DELETE_BY_ID)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean existWithId(Connection connection, int id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        return rs.next();
    }

    private boolean existWithName(Connection connection, String name) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_BY_NAME);
        statement.setString(1, name);
        ResultSet rs = statement.executeQuery();
        rs.next();
        return name.equalsIgnoreCase(rs.getString("name"));
    }
}
