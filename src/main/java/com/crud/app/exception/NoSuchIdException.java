package com.crud.app.exception;

public class NoSuchIdException extends DBException {

    public NoSuchIdException(String message) {
        super(message);
    }

    public NoSuchIdException(String message, Throwable cause) {
        super(message, cause);
    }
}
