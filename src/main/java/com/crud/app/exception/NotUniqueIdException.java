package com.crud.app.exception;

public class NotUniqueIdException extends DBException {

    public NotUniqueIdException(String message) {
        super(message);
    }

    public NotUniqueIdException(String message, Throwable cause) {
        super(message, cause);
    }
}
