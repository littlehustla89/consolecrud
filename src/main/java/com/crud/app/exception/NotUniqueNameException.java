package com.crud.app.exception;

public class NotUniqueNameException extends DBException {

    public NotUniqueNameException(String message) {
        super(message);
    }

    public NotUniqueNameException(String message, Throwable cause) {
        super(message, cause);
    }
}
