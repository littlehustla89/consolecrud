package com.crud.app.model;

import java.util.List;

public class Group extends BaseObject{

    protected String description;
    protected String region;
    protected List<Provider> providers;

    public Group(int id, String name, String description, String region) {
        super(id, name);
        this.description = description;
        this.region = region;
    }

    public Group(String description, String region) {
        this.description = description;
        this.region = region;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List<Provider> getProviders() {
        return providers;
    }

    public void setProviders(List<Provider> providers) {
        this.providers = providers;
    }

    @Override
    public String toString() {
        return "Group{" +
                "description='" + description + '\'' +
                ", region='" + region + '\'' +
                ", providers=" + providers +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
