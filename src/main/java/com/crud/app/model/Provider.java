package com.crud.app.model;

public class Provider extends BaseObject {

    protected String providerDescription;
    protected String serviceName;
    protected Long servicePrice;
    protected Integer groupId;

    public Provider(int id, String name, String providerDescription, String serviceName, Long servicePrice) {
        super(id, name);
        this.providerDescription = providerDescription;
        this.serviceName = serviceName;
        this.servicePrice = servicePrice;
    }

    public Provider(String providerDescription, String serviceName, Long servicePrice) {
        this.providerDescription = providerDescription;
        this.serviceName = serviceName;
        this.servicePrice = servicePrice;
    }

    public String getProviderDescription() {
        return providerDescription;
    }

    public void setProviderDescription(String providerDescription) {
        this.providerDescription = providerDescription;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Long getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(Long servicePrice) {
        this.servicePrice = servicePrice;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "Provider{" +
                "providerDescription='" + providerDescription + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", servicePrice=" + servicePrice +
                ", groupId=" + groupId +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
