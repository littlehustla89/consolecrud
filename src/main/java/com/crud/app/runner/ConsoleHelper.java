package com.crud.app.runner;

import com.crud.app.controller.Controller;
import com.crud.app.controller.impl.GroupController;
import com.crud.app.controller.impl.ProviderController;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.view.GroupView;
import com.crud.app.view.ProviderView;
import com.crud.app.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private View view;
    private Controller controller;

    private ConsoleHelper() {
    }

    public static ConsoleHelper getHelper() {
        return new ConsoleHelper();
    }

    /**
     * main method to start application
     */
    public void startConsoleApplication() {
        showEntitiesMenu();
    }

    /**
     * method shows all entities to select
     */
    private void showEntitiesMenu() {
        while (true) {
            try {
                writeToConsole("\nSelect entity:\n\n" +
                        "0 - Group\n" +
                        "1 - Provider\n" +
                        "2 - Exit\n");
                switch (Integer.parseInt(readString())) {
                    case 0:
                        view = new GroupView();
                        controller = new GroupController();
                        view.setController(controller);
                        showCommandsMenu();
                        break;
                    case 1:
                        view = new ProviderView();
                        controller = new ProviderController();
                        view.setController(controller);
                        showCommandsMenu();
                        break;
                    case 2:
                        System.out.println("See you later");
                        return;
                    default:
                        throw new IllegalArgumentException();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                writeToConsole("Wrong select. Try again.");
            }
        }
    }

    private void showCommandsMenu() {
        while (true) {
            try {
                writeToConsole("\nSelect command:\n\n" +
                        "0 - Create\n" +
                        "1 - Select all\n" +
                        "2 - Select by id\n" +
                        "3 - Update by id\n" +
                        "4 - Delete by id\n" +
                        "5 - Back\n");
                switch (Integer.parseInt(readString())) {
                    case 0:
                        view.fireEventCreate();
                        ConsoleHelper.writeToConsole("Command executed successfully.");
                        break;
                    case 1:
                        view.fireEventGetAll();
                        break;
                    case 2:
                        view.fireEventGetById();
                        break;
                    case 3:
                        view.fireEventUpdate();
                        ConsoleHelper.writeToConsole("Command executed successfully.");
                        break;
                    case 4:
                        view.fireEventDelete();
                        ConsoleHelper.writeToConsole("Command executed successfully.");
                        break;
                    case 5:
                        return;
                    default:
                        throw new IllegalArgumentException();
                }
            } catch (IOException | NotUniqueNameException | NotUniqueIdException e) {
                writeToConsole(e.getMessage());
            } catch (IllegalArgumentException e) {
                writeToConsole(e.getMessage());
                writeToConsole("Wrong command. Try again.");
            }
        }
    }

    public static String readString() throws IOException {
        return reader.readLine();
    }

    public static void writeToConsole(String message) {
        System.out.println(message);
    }
}
