package com.crud.app.service;

import com.crud.app.model.Group;

public interface GroupService extends Service<Group> {
}
