package com.crud.app.service;

import com.crud.app.model.Provider;

public interface ProviderService extends Service<Provider> {
}
