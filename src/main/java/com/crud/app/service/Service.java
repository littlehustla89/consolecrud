package com.crud.app.service;

import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.model.BaseObject;

import java.util.List;

public interface Service<T extends BaseObject> {

    void createObject(T object) throws NotUniqueIdException, NotUniqueNameException;

    T getObjectById(int id) throws NoSuchIdException;

    List<T> getAll();

    void updateObject(T object) throws NotUniqueNameException, NotUniqueIdException;

    void deleteObject(int id);
}
