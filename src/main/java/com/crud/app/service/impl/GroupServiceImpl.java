package com.crud.app.service.impl;

import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.dao.GroupDao;
import com.crud.app.dao.jdbc.JdbcGroupDaoImpl;
import com.crud.app.model.Group;
import com.crud.app.service.GroupService;

import java.util.List;

public class GroupServiceImpl implements GroupService {

    protected GroupDao dao;

    public GroupServiceImpl() {
        dao = new JdbcGroupDaoImpl();
    }

    @Override
    public void createObject(Group object) throws NotUniqueIdException, NotUniqueNameException {
        dao.create(object);
    }

    @Override
    public Group getObjectById(int id) throws NoSuchIdException {
        return dao.getById(id);
    }

    @Override
    public List<Group> getAll() {
        return dao.getAll();
    }

    @Override
    public void updateObject(Group object) throws NotUniqueNameException, NotUniqueIdException {
        dao.update(object);
    }

    @Override
    public void deleteObject(int id) {
        dao.delete(id);
    }
}
