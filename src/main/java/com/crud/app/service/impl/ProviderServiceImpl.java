package com.crud.app.service.impl;

import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.dao.ProviderDao;
import com.crud.app.dao.jdbc.JdbcProviderDaoImpl;
import com.crud.app.model.Provider;
import com.crud.app.service.ProviderService;

import java.util.List;

public class ProviderServiceImpl implements ProviderService {

    protected ProviderDao dao;

    public ProviderServiceImpl() {
        dao = new JdbcProviderDaoImpl();
    }

    @Override
    public void createObject(Provider object) throws NotUniqueIdException, NotUniqueNameException {
        dao.create(object);
    }

    @Override
    public Provider getObjectById(int id) throws NoSuchIdException {
        return dao.getById(id);
    }

    @Override
    public List<Provider> getAll() {
        return dao.getAll();
    }

    @Override
    public void updateObject(Provider object) throws NotUniqueNameException, NotUniqueIdException {
        dao.update(object);
    }

    @Override
    public void deleteObject(int id) {
        dao.delete(id);
    }
}
