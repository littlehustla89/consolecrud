package com.crud.app.view;

import com.crud.app.controller.Controller;
import com.crud.app.exception.NoSuchIdException;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;
import com.crud.app.model.Provider;
import com.crud.app.runner.ConsoleHelper;

import java.io.IOException;
import java.util.List;

public class ProviderView implements View {

    private Controller<Provider> controller;

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void fireEventCreate() throws NotUniqueNameException, NotUniqueIdException {
        ConsoleHelper.writeToConsole("Creating Provider object ...");
        controller.onCreate(createProvider());
    }

    @Override
    public void fireEventGetById() {
        while (true) {
            ConsoleHelper.writeToConsole("Input desired ID:");
            try {
                int id = Integer.parseInt(ConsoleHelper.readString());
                writeById(controller.onGetById(id));
                return;
            } catch (IOException e) {
                ConsoleHelper.writeToConsole("Wrong ID. Try again.\n");
            } catch (NoSuchIdException e) {
                ConsoleHelper.writeToConsole(e.getMessage());
            }
        }
    }

    @Override
    public void fireEventGetAll() {
        writeAll(controller.onGetAll());
    }

    @Override
    public void fireEventUpdate() throws NotUniqueNameException, NotUniqueIdException {
        ConsoleHelper.writeToConsole("Creating Prodiver object for update...");
        controller.onUpdate(createProvider());

    }

    @Override
    public void fireEventDelete() {
        while (true) {
            ConsoleHelper.writeToConsole("Input desired ID:");
            try {
                int id = Integer.parseInt(ConsoleHelper.readString());
                controller.onDelete(id);
                return;
            } catch (IOException e) {
                ConsoleHelper.writeToConsole("Wrong ID. Try again.\n");
            }
        }
    }

    private void writeById(Provider provider) {
        if (provider == null || (provider.getId() == 0 && provider.getName() == null)) {
            ConsoleHelper.writeToConsole("\nThere is no such ID\n");
        } else {
            ConsoleHelper.writeToConsole("\n" + provider.toString() + "\n");
        }
    }

    private void writeAll(List<Provider> list) {
        if (list.isEmpty()) {
            ConsoleHelper.writeToConsole("\nThere are no records to view.\n");
        } else {
            ConsoleHelper.writeToConsole("\nAll records of table 'provider'\n");
            for (Provider provider : list) {
                ConsoleHelper.writeToConsole(provider.toString());
            }
            ConsoleHelper.writeToConsole("\n");
        }
    }

    private Provider createProvider() {
        int id;
        String name;
        String providerDescription;
        String serviceName;
        Long servicePrice;
        Integer groupId;

        while (true) {
            try {
                ConsoleHelper.writeToConsole("Input id (if you want auto increment for 'Creating', input zero):");
                id = Integer.parseInt(ConsoleHelper.readString());
                break;
            } catch (IOException | NumberFormatException e) {
                ConsoleHelper.writeToConsole("Wrong integer. Try again");
            }
        }
        while (true) {
            try {
                ConsoleHelper.writeToConsole("Input  name:");
                name = ConsoleHelper.readString();
                break;
            } catch (IOException e) {
                ConsoleHelper.writeToConsole("Failed input. Try again");
            }
        }
        while (true) {
            try {
                ConsoleHelper.writeToConsole("Input  provider description:");
                providerDescription = ConsoleHelper.readString();
                break;
            } catch (IOException e) {
                ConsoleHelper.writeToConsole("Failed input. Try again");
            }
        }
        while (true) {
            try {
                ConsoleHelper.writeToConsole("Input  service name:");
                serviceName = ConsoleHelper.readString();
                break;
            } catch (IOException e) {
                ConsoleHelper.writeToConsole("Failed input. Try again");
            }
        }
        while (true) {
            try {
                ConsoleHelper.writeToConsole("Input  service price:");
                servicePrice = Long.parseLong(ConsoleHelper.readString());
                break;
            } catch (IOException | NumberFormatException e) {
                ConsoleHelper.writeToConsole("Wrong Long. Try again");
            }
        }
        while (true) {
            try {
                ConsoleHelper.writeToConsole("Input group id:");
                groupId = Integer.parseInt(ConsoleHelper.readString());
                break;
            } catch (IOException | NumberFormatException e) {
                ConsoleHelper.writeToConsole("Wrong Integer. Try again");
            }
        }
        Provider provider = new Provider(id, name, providerDescription, serviceName, servicePrice);
        provider.setGroupId(groupId);
        return provider;
    }
}
