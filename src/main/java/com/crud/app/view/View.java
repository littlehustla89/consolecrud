package com.crud.app.view;

import com.crud.app.controller.Controller;
import com.crud.app.exception.NotUniqueIdException;
import com.crud.app.exception.NotUniqueNameException;

public interface View {

    void setController(Controller controller);

    void fireEventCreate() throws NotUniqueNameException, NotUniqueIdException;

    void fireEventGetById();

    void fireEventGetAll();

    void fireEventUpdate() throws NotUniqueNameException, NotUniqueIdException;

    void fireEventDelete();
}
