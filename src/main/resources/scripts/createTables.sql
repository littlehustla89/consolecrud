create table IF NOT EXISTS public.group_table
(
  id           SERIAL PRIMARY KEY,
  name   varchar(255),
  description   varchar(255),
  region    varchar(255)
);

create table IF NOT EXISTS public.provider_table
(
  id            SERIAL PRIMARY KEY,
  name varchar(255),
  provider_description varchar(255),
  service_name varchar(255),
  service_price   bigint,
  group_id     int
                constraint fk_group_id
                references group_table
);
